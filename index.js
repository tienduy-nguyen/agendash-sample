const express = require('express');
const app = express();
const Agenda = require('agenda');
const Agendash = require('agendash');
const connectDB = require('./config/db');

//Init middleware
app.use(express.json({ extends: false }));

//Connect database
connectDB();

//Root path of API
app.use('/', (req, res) => res.send('API running'));

(async function run() {
  let configureMongoDBObj = {
    db: {
      address: process.env.MONGODB_URI,
      collection: 'jobs',
      options: {
        useNewUrlParser: true,
      },
    },
  };

  let agenda = new Agenda(configureMongoDBObj);
  agenda.define('JOB_ONE', (job, done) => {
    console.log('job one fired here');
    done();
  });

  agenda.define('JOB_TWO', (job, done) => {
    console.log('JOB TWO fired here');
    done();
  });

  agenda.define('JOB_THREE', (job, done) => {
    console.log('JOB three in every minute here');
    done();
  });
  //agendash UI config
  app.use('/dash', Agendash(agenda));
  await agenda.start();
  //schedule once
  await agenda.schedule('1 minute', 'JOB_ONE');
  await agenda.schedule('2 minute', 'JOB_ONE');
  await agenda.schedule('3 minute', 'JOB_TWO');
  await agenda.schedule('4 minute', 'JOB_THREE');
  //repeat
  await agenda.every('1 minute', 'JOB_3');
})();

const PORT = process.env.PORT || 2810;
app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
});
